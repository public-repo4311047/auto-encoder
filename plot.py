from matplotlib import pyplot as plt


def plot(x_output):
    # Plot images (note requires pyside6 or pyqt5 for displaying)

    n = x_output.shape[0]
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # display original
        # ax = plt.subplot(2, n, i + 1)
        # plt.imshow(x_input[i])
        # plt.title("original")
        # plt.gray()
        # ax.get_xaxis().set_visible(False)
        # ax.get_yaxis().set_visible(False)

        # display reconstruction
        ax = plt.subplot(2, n, i + 1 + n)
        plt.imshow(x_output[i])
        plt.title("decoded")
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

    plt.show()