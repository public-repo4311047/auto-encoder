class Settings:
    # Number of training rounds
    TRAINING_EPOCHS = 2
    # Limit the number of input samples,
    # limiting can be useful during frequent testing of the program
    # Default -1 to get all
    TRAINING_MAX_SAMPLES = -1
    # Number of features in the encoded output
    MODEL_LATENT_DIMENSION = 8
