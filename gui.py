from random import randint

import numpy as np
from PySide6.QtCore import Qt, QTimer
from PySide6.QtGui import QImage, QPixmap
from PySide6.QtWidgets import QWidget, QVBoxLayout, QMainWindow, QLabel, QSlider, QPushButton

from auto_encoder import AutoEncoder
from data_set import load_data
from settings import Settings


class GuiApplication(QMainWindow):
    def __init__(self):
        super().__init__()
        x_train, x_test = load_data()
        shape = x_train[0].shape

        self.auto_encoder = AutoEncoder(shape=shape, latent_dimension=Settings.MODEL_LATENT_DIMENSION)
        self.auto_encoder.train(x_train[:Settings.TRAINING_MAX_SAMPLES], x_test, epochs=Settings.TRAINING_EPOCHS)

        # Take one encoded example as starting point for the sliders
        # encoded_images = self.auto_encoder.encode(np.array([x_test[0]]))
        self.sample_encoded_images = self.auto_encoder.encode(x_test)
        # global_max = np.amax(encoded_images)
        self.encoded_data = self.sample_encoded_images[0]

        self.slider_widgets = []
        self.image_view = QLabel(self)

        # start with some data to have dimensions, will be immediately overwritten
        self.decoded_image = x_train[0]

        self.init_ui()

        self.refresh_needed = True
        # Set up a QTimer for the cyclic background task
        self.background_task_timer = QTimer(self)
        self.background_task_timer.timeout.connect(self.background_task)
        self.background_task_timer.start(500)  # Interval in milliseconds

    def load_random(self):
        """ Load a new random image """
        min_value = 0
        max_value = self.sample_encoded_images.shape[0]
        random_index = randint(min_value, max_value)
        self.encoded_data = self.sample_encoded_images[random_index]
        self.refresh_now()

    def init_ui(self):
        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        layout = QVBoxLayout(central_widget)

        button = QPushButton('Random sample image', self)
        button.clicked.connect(self.load_random)
        layout.addWidget(button)

        layout.addWidget(self.image_view)

        # Array editor part
        for index, value in enumerate(self.encoded_data):
            # Slider only does int
            slider_conversion_factor = 100
            slider_value = value * slider_conversion_factor

            label = QLabel(f"Value {index}: {value}")
            slider = QSlider(Qt.Horizontal)
            slider.setMinimum(0)
            slider.setMaximum(32 * slider_conversion_factor)
            slider.setValue(slider_value)
            slider.valueChanged.connect(lambda v=slider_value, f=slider_conversion_factor, i=index:
                                        self.slider_changed(v, f, i))

            layout.addWidget(label)
            layout.addWidget(slider)

            self.slider_widgets.append((slider, label))

        self.setGeometry(100, 100, 600, 400)
        self.setWindowTitle('Auto encoder editor')

    def slider_changed(self, slider_value, slider_conversion_factor, index):
        # Update corresponding text label
        original_value = slider_value / slider_conversion_factor
        label = self.slider_widgets[index][1]
        label.setText(f"Value {index}: {original_value}")
        self.encoded_data[index] = original_value
        # request refresh of the image
        self.request_refresh()

    def request_refresh(self):
        """
        Do not immediately refresh on slider change because of performance
        """
        self.refresh_needed = True

    def refresh_now(self):
        self.decoded_image = self.auto_encoder.decode(np.array([self.encoded_data]))[0]
        self.image_view.setPixmap(self.array_to_image())

    def background_task(self):
        if self.refresh_needed:
            self.refresh_now()

    def array_to_image(self, scale_factor=8.0):
        """
        Convert the numpy 2d array of floats to an image that QT can display
        """
        height, width = self.decoded_image.shape

        # Convert float values to integer values in the range [0, 255]
        normalized_array = (self.decoded_image * 255).astype(np.uint8)

        q_image = QImage(normalized_array, width, height, QImage.Format_Grayscale8)

        pixmap = QPixmap.fromImage(q_image)
        scaled_image = pixmap.size() * scale_factor

        return pixmap.scaled(scaled_image)
