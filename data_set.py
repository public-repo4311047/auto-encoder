from tensorflow.keras.datasets import fashion_mnist


def load_data():
    # 10000 greyscale 28x28 images https://www.tensorflow.org/datasets/catalog/fashion_mnist
    (x_train, _), (x_test, _) = fashion_mnist.load_data()

    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.

    return x_train, x_test
