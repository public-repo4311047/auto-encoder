import tensorflow as tf

from tensorflow.keras import layers, losses
from tensorflow.keras.models import Model


class AutoencoderModel(Model):
    """
    Model definition used by the AutoEncoder class,
    built upon tutorial from https://www.tensorflow.org/tutorials/generative/autoencoder
    """

    def __init__(self, latent_dim, shape):
        super().__init__()
        self.latent_dim = latent_dim
        self.shape = shape
        self.encoder = tf.keras.Sequential([
            layers.Flatten(),
            layers.Dense(latent_dim, activation='relu'),
        ])
        self.decoder = tf.keras.Sequential([
            layers.Dense(tf.math.reduce_prod(shape), activation='sigmoid'),
            layers.Reshape(shape)
        ])

    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded


class AutoEncoder:
    def __init__(self, shape, latent_dimension=64):
        """
        Auto encoder
        Two neural networks,
            encoder network: to convert input of shape to new dimension (latent_dimension)
            decoder network: to do the inverse

        :param shape: the dimensions of the input (and output) for example 28x28 pixels
        :param latent_dimension: the number of features after encoding
        """

        self.autoencoder = AutoencoderModel(latent_dimension, shape)
        # Set the auto encoder properties
        self.autoencoder.compile(optimizer='adam',
                                 loss=losses.MeanSquaredError())

    def encode(self, x_test):
        """
        Convert input data (input as in the trained data, or test data)
        to encoded data with latent_dimension as size

        :param x_test: numpy array of elements with size shape
        """
        return self.autoencoder.encoder(x_test).numpy()

    def decode(self, x_encoded):
        """
        Convert encoded data with latent_dimension to data in the original shape

        :param x_encoded: numpy array of elements with size latent_dimension
        """
        return self.autoencoder.decoder(x_encoded).numpy()

    def train(self, x_train, x_test, epochs=1):
        self.autoencoder.fit(x_train,
                             x_train,
                             epochs=epochs,  # training rounds/epochs
                             shuffle=True,
                             validation_data=(x_test, x_test))
