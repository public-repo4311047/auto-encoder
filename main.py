import sys

from PySide6.QtWidgets import QApplication

from gui import GuiApplication


# Made upon: https://www.tensorflow.org/tutorials/generative/autoencoder


if __name__ == '__main__':
    app = QApplication(sys.argv)

    # Create and show the main window
    window = GuiApplication()
    window.show()

    sys.exit(app.exec())
